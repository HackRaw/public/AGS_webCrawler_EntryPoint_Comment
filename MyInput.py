# -*- coding: utf-8 -*-

class MyInput:
    """Class that represent a web page"""
 
    id = ""
    name = ""
    id_input = 0

    def __init__(self, id_input, id, name):
        self.id_input = id_input # Just an index
        self.id = id        # Retrieved from the HTML code
        self.name = name    # Retrieved from the HTML code
    
    def get_id(self):
        return self.id

    def get_name(self):
        return self.name

    def to_string(self):
        return ("|__" + "ID Input#" + str(self.id_input) + 
                "\n\t\t|__" + "id :" + str(self.id) +
                "\n\t\t|__" + "name: " + str(self.name) +
                "\n\n")