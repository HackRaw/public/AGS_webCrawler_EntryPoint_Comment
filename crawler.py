#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# https://urllib3.readthedocs.io/en/latest/
import urllib3

# beautiful soup py3
# apt install python3-bs4 
from bs4 import BeautifulSoup

# Need lxml parser
# #apt-get install python-lxml 
from bs4 import SoupStrainer
import sys
import requests
import time 
from urllib.parse import urlparse
from requests.exceptions import ConnectionError


url = 'http://sd.local/control-center'
# url = 'http://localhost/TestScrawler'
url = 'http://sd.local/control-center'

if url[:-1] != "/":
    url += "/"

parsed_uri = urlparse(url)
TLD = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
# print(TLD)

urlToScrawl = []
urlOutOfScope = []
urlAlreadyVisited=[]
formAlreadySeen=[]
arrayForm = []
arrayForm_size = 0

MyCookies = {'SD_cc_user': '%227345%22', 'SD_cc_session': '%220c1e99cb97911eaf3d7b5598887659af%22', 'SD_user_id':'7345', 'SD_user_token':'69d690603030fb0ab29c267ba9d0f988', 'SD_company_id':'1'}
#MyCookies = {'':''}

def download_page(url):
    print("Currently on: " + url)
    r = requests
    r.content =  "No response"

    try:
        r = requests.get(url, cookies=MyCookies, timeout=15)
    except ConnectionError as e:    # This is the correct syntax
        print("BAD CONNECTION TIMEOUT")

    urlAlreadyVisited.append(url)
    return r


def get_all__links_on_the_page(page):

    soup = BeautifulSoup(page.content, 'html.parser')
    links = soup.find_all('a')
  
    for l in links:
        href = l.get('href')

        if href is not None and "/" in href and not "mailto" in href and not "function()" in href:

            if href[0] == "/":
                href = TLD[:-1] + href
           
            currentURL = urlparse(href)
            TLDcurrentURL = '{uri.scheme}://{uri.hostname}/'.format(uri=currentURL) 
            
            # Same domain ?
            if TLD != TLDcurrentURL:
                if urlOutOfScope.count(href) == 0:
                    urlOutOfScope.append(href)

            # Avoid bad link (n)
            elif "logout" in href.lower() or "console" in href.lower() or "promote" in href.lower() or "update-sm" in href.lower() or "rss" in href.lower():
                if urlAlreadyVisited.count(href) == 0:
                    urlAlreadyVisited.append(href)

            # Url had param ?: Discard
            elif "?" in href:
                 if urlAlreadyVisited.count(href) == 0:
                    urlAlreadyVisited.append(href)

            # Store the link
            elif urlAlreadyVisited.count(href) == 0 and urlToScrawl.count(href)== 0:
                urlToScrawl.append(href)
    urlToScrawl.reverse()


def get_all_form(page, url):
    soup = BeautifulSoup(page.content, 'html.parser')
    inputs = soup.find_all('form')
    for i in inputs:
        # print('FORM: ' + str(i))
        
        if formAlreadySeen.count(i) == 0:
            formAlreadySeen.append(i)
            arrayForm.append([])
            arrayForm[len(arrayForm)-1].append(str(url))
            # print('add')
            # print(arrayForm)
        else:
            idx = get_index_form_alrdy_seen(formAlreadySeen, i)
            # print('arrayForm_size ' + str(len(arrayForm)))
            # print("len(formAlreadySeen) " + str(len(formAlreadySeen)))
            # print(arrayForm)
            
            if url not in arrayForm[idx]:
                arrayForm[idx].append(str(url))
            else:
                print("DEJA VU !!")
        # print('\n\n\n')

def get_index_form_alrdy_seen(list_form, form):
    index = 0
    ret = 0
    for i in list_form:
        if i == form:
            ret = index
        else:
            index +=1
    return ret

def main():
    
      # First gathering
    page = download_page(url)
    get_all__links_on_the_page(page)
    get_all_form(page, url)

    while urlToScrawl:
        currentURL = urlToScrawl.pop()
        page = download_page(currentURL)
        get_all__links_on_the_page(page)
        get_all_form(page, currentURL)


    print("")
    print("")

    index = 0
    print("LINKAGE")
    for i in arrayForm:
        print("ID_FORM: " + str(index))
        #print("===============\n\n\n" + str(formAlreadySeen[index]) + "\n\n\n===============\n\n\n")
        for a in arrayForm[index]:
            print(a)
        index += 1
        print("")

    print("\n\n")
    print("\n\n")
    print("\n\n")
    
    urlOutOfScope.sort()
    urlAlreadyVisited.sort()

    # for i in urlOutOfScope:
    #     print("Out of scope: " + i)
    # print("")
    # for i in urlAlreadyVisited:
    #     print("Already: " + i)
    # print("")

if __name__ == '__main__':
    main()