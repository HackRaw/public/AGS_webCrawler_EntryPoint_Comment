# -*- coding: utf-8 -*-

from MyInput import MyInput
from bs4 import BeautifulSoup


class EntryPoint(object):
    typeEntry ="" # Form, GET param, etc...
    location = "" # URL
    inputs_list = []
    method = ""   # POST or GET or PUT, ...etc...
    action = ""   # What the form do ?
    raw_data = ""
    id_form = 0
    id_input = 0

    def __init__(self, id_form, typeEntry, method, location, action, raw_data):
            self.id_input = 0
            self.id_form = id_form
            self.inputs_list =[]
            self.typeEntry = typeEntry
            self.method = str(method).upper()
            self.location = location
            self.action = action
            self.raw_data = "<html><head><title>Stupid/title></head><body>raw_data" + str(raw_data) + "</body></html>"
            self.get_input()

    def get_input(self):
        soup = BeautifulSoup(self.raw_data, 'html.parser')
        inputs = soup.find_all("input")
        #print(inputs)
        
        #print("New input: " + self.location)
        for i in inputs:
            self.inputs_list.append(MyInput(self.id_input, i.get('id'), i.get('name')))
            self.id_input = self.id_input + 1
        
        
    def to_string(self):
        inputs = ""
        
        for i in self.inputs_list:
            inputs += "\t" + i.to_string()

        return ("===========================< ENTRY POINT #" +str(self.id_form) + " >===========================" +
                "\n" + "Action: " + str(self.action) + 
                "\n" + "Location: " + str(self.location) + # str(self) +
                "\n" + "Method: " + str(self.method) +
                "\n" + "Nb input: " +str(len(self.inputs_list)) +
                "\n" + inputs)
