# A basic Crawler for entry point and comment in page source code

This tool aims to index every entry point of a website by crawling the website.
Each time the script found a form, an input field or something that let the user to provide data: the url is displayed.
Each time the script found a comment, the comment is stored on text file for futher investigagion (Still to do)

Very useful to map application in pentest(find XSS, SQLI, Code injection)

![ScreenShot](https://i.imgur.com/a9hueZL.png)
