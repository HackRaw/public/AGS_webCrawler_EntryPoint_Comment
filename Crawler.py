# -*- coding: utf-8 -*-


# from HTMLParimport html.parserser import HTMLParser
import html.parser


import sys
import requests
import time 

from requests.exceptions import ConnectionError
from bs4 import BeautifulSoup

from EntryPoint import EntryPoint

class Crawler(object):
    """ The crawler class"""

    TLD = ""
    my_cookies = {}

    url_already_visited = []
    url_to_visit = []
    url_out_of_scope = []
    links_on_the_page = []

    current_page = "" 
    current_page_raw_data = ""

    entry_points = []
    id_form = 0

    

    def __init__(self, cookies, TLD):
        self.TLD = TLD
        self.my_cookies = cookies
        
        url_already_visited = []
        url_to_visit = []
        url_out_of_scope = []
        links_on_the_page = []

        current_page = "" 
        current_page_raw_data = ""

        entry_points = []
        id_form = 0


    def download_page(self, url):
        """This function download the page"""
        self.current_url = url
        r = requests.get(url, cookies=self.my_cookies, timeout=15)

        self.url_already_visited.append(url)
        
        self.current_page_raw_data = r.text        
        self.extract_all_links()
        self.extract_all_forms()


    def extract_all_forms(self):
        soup = BeautifulSoup(self.current_page_raw_data, 'html.parser')
        forms = soup.find_all("form")     
        print("NB form on this page: " + str(len(forms)))

        for i in forms:
            # Create new object entry point
            self.entry_points.append(EntryPoint(self.id_form, "FORM",i.get('method'), self.current_url,i.get('action'), i))
            self.id_form = self.id_form + 1


    def extract_all_links(self):
        """ test"""
        self.links_on_the_page = []

        soup = BeautifulSoup(self.current_page_raw_data, 'html.parser')
        links = soup.find_all("a")       
        
        for i in links:
            href = i.get('href')
            if href is not "":
                # Link to JS script
                if ( "javascript" not in href and
                "#" not in href and
                "logout" not in href and 
                "?" not in href and
                "remove" not in href):
                    if href[0] == "/":
                        href = self.TLD[:-1] + href
                    self.links_on_the_page.append(href.strip())
        self.add_url()


    def add_url(self):
        print("Links nb:" + str(len(self.links_on_the_page)))
        for i in self.links_on_the_page:
            #print("ADDURL: " +str(i))
            if self.TLD == i[0:len(self.TLD)]:
               
                if (i not in str(self.url_to_visit) and
                    (i not in str(self.url_already_visited)) and 
                    (i not in str(self.url_out_of_scope))):   

                    # print(str(i) + ' => added to visit')
                    self.url_to_visit.append(i)
            else:
                self.url_out_of_scope.append(i)

        self.links_on_the_page = []


    def get_entry_points(self):
        for i in self.entry_points:
            print(i.to_string())
            

    def get_url_visited(self):
        for i in self.url_already_visited:
            print(i)
    
    def get_url_to_visit(self):
        for i in self.url_to_visit:
            print(i) 

    def get_url_out_of_scope(self):
        for i in self.url_out_of_scope:
            print(i) 

    def run(self):
        idx = 0 
        while self.url_to_visit:
            print("Iteration: " + str(idx))
            
            url = self.url_to_visit.pop()
            self.download_page(url)
            print("current: " + self.current_url)

            print("")
            print("Out of scope:")
            self.get_url_out_of_scope()
            
            print("")
            print("url visited")
            self.get_url_visited()
                
            print("")
            print("To visit:")
            self.get_url_to_visit()
            print("===================================\n\n")
            idx +=1
            #time.sleep(2)
        self.get_entry_points()
